﻿using System.Web;
using System.Web.Optimization;

namespace LVL2_ASPNet_MVC_02
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/vendor/jquery/jquery.min.js",
                      "~/Content/vendor/bootstrap/js/bootstrap.bundle.min.js",
                      "~/Content/vendor/jquery-easing/jquery.easing.min.js",
                      "~/Content/vendor/magnific-popup/jquery.magnific-popup.min.js",
                      "~/Scripts/js/creative.min.js"));

  
            bundles.Add(new StyleBundle("~/bundels/css").Include(
                      "~/Content/vendor/fontawesome-free/css/all.min.css",
                      "~/Content/vendor/magnific-popup/magnific-popup.css",
                      "~/Content/css/creative.min.css"));
        }
    }
}
